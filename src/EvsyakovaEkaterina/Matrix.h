#pragma once
#include <time.h>
#include <stdlib.h>
#include <iostream>  
using namespace std;

class Matrix
{
private:
	int rows, collumns;
	double **cmatrix;
public:
	Matrix(int a, int b)
	{
		rows = a;
		collumns = b;

		cmatrix = new double*[a];
		for (int i = 0; i < a; i++)
		{
			cmatrix[i] = new double[b];
		}
	}

	Matrix(const Matrix & matrix)
	{
		rows = matrix.rows;
		collumns = matrix.collumns;
		cmatrix = new double*[rows];
		for (int i = 0; i < rows; i++)
			cmatrix[i] = new double[collumns];

		for (int i = 0; i < rows; i++)
			for (int j = 0; j < collumns; j++)
				cmatrix[i][j] = matrix[i][j];
	}

	~Matrix()
	{
		for (int i = 0; i < rows; i++)
		{
			delete[] cmatrix[i];
		}
		delete[] cmatrix;
	}

	friend ostream& operator<<(ostream& os, const Matrix& matrix)
	{
		for (int i = 0; i < matrix.collumns; i++)
		{
			for (int j = 0; j < matrix.rows; j++)
				os << matrix(i, j) << " ";
			os << endl;
		}
		return os;
	}

	friend istream& operator >> (istream& is, Matrix& matrix)
	{
		for (int i = 0; i < matrix.collumns; i++)
		{
			for (int j = 0; j < matrix.rows; j++)
				is >> matrix(i, j);
		}
		return is;
	}

	double*&  operator[](int index) const
	{
		return cmatrix[index];
	}


	double & operator()(int i, int j) const
	{
		if (i < 0 || i >= rows || j < 0 || j >= collumns)
			throw 1;
		return cmatrix[i][j];
	}


	Matrix operator+(const Matrix & matrix)
	{
		Matrix tmp(rows, collumns);
		if (rows != matrix.rows || collumns != matrix.collumns)
			throw 1;
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < collumns; j++)
				tmp(i, j) = matrix[i][j] + matrix[i][j];
		return tmp;
	}

	Matrix operator-(const Matrix & matrix)
	{
		Matrix tmp(rows, collumns);
		if (rows != matrix.rows || collumns != matrix.collumns)
			throw 1;
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < collumns; j++)
			{
				tmp(i, j) = cmatrix[i][j] - matrix[i][j];
			}
		}
		return tmp;
	}

	Matrix operator*(const Matrix & matrix)
	{
		Matrix tmp(rows, matrix.collumns);
		if (collumns != matrix.rows)
			throw 1;
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < matrix.collumns; j++)
			{
				tmp(i, j) = 0;
				for (int k = 0; k < collumns; k++)
					tmp.cmatrix[i][j] += (matrix[i][k] * matrix.cmatrix[k][j]);
			}
		return tmp;
	}

	Matrix operator*(const double & constanta)
	{
		Matrix tmp(rows, collumns);
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < collumns; j++)
				tmp(i, j) = cmatrix[i][j] * constanta;
		return tmp;
	}

	Matrix operator/(const double & constanta)
	{
		Matrix tmp(rows, collumns);
		if (constanta == 0)
			throw 1;
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < collumns; j++)
				tmp(i, j) = cmatrix[i][j] / constanta;
		return tmp;
	}


	Matrix& operator=(const Matrix & matrix)
	{
		if (&matrix == this)
			return *this;
		if (matrix.collumns != collumns || matrix.rows != rows)
			throw 1;
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < collumns; j++)
				matrix[i][j] = matrix[i][j];
		return *this;
	}


	bool operator==(const Matrix & matrix) const
	{
		if (rows != matrix.rows || collumns != matrix.collumns)
			return false;
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < collumns; j++)
				if (cmatrix[i][j] != matrix[i][j])
					return false;
		}
		return true;
	}

	bool operator!=(const Matrix & matrix) const
	{
		return !(*this == matrix);
	}
};