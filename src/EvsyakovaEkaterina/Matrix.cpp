#include "Matrix.h"
#include <fstream>
#define num 3
using namespace std;
int main()
{
	srand(time(0));
	ifstream input("input.txt");
	ofstream output("output.txt");
	Matrix R(3, 3), Q(3, 3);
	input >> R >> Q;
	output << R << endl << Q;
	output << endl << "The sum of the matrices" << endl << R + Q << endl;
	output << endl << "The subtraction of matrices" << endl << R - Q << endl;
	output << endl << "The multiplication of matrices" << endl << R*Q << endl;
	output << "Thu multiplication first matrix by number " << num << "=" << endl << R*num << endl;
	output << "The devide first mutrix by number " << num << "=" << endl << R / num << endl;
	if (R == Q)
		output << "First matrix is equal to second matrix" << endl;
	else if (R != Q)
		output << "First matrix isn`t equal to second matrix" << endl;
	return 0;
}